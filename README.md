# README #

The program presents space-x missions progress.
Report describes in general way achievements and improvements between 2006-2020.

Results based on open space-x API. I took into consideration two endpoints:

- payloads

- launches

I decided to download data from these two endpoints and use data locally in the project.
I wanted to avoid situation when end user has problem with connectivity/proxies.
Thanks that data are always present.

# METRICS #

Question is general so I decided to choose 5 quite general metrics:

- Successful launches in every year between 2006 and 2020

- Failed launches in every year between 2006 and 2020

- Total launches in every year between 2006 and 2020

- Unique customers in every year between 2006 and 2020

- Payloads in kilograms in every year between 2006 and 2020


# Tests #

A few unittests were added.
To run tests you will need pytest.

Command: python -m pytest .

# Requirements #

All packages used by script are in requirements.txt file


Command: python -m pip install -r requirements.txt

# How to run report generation #

To run report:

python main.py

To install dependencies and run report generation you can use 'run.bat' script.

Results will be generated in report catalog. Report is in HTML file.

Steps in run.bat script:

- requirements installation

- run tests

- run main program


# CONCLUSIONS #

All metrics are presented in histograms so in the first glace end user can see progress.
Trend is visable in each histogram and data shows improvements in each chosen category.

"Is the space program doing okay?"

I would answer: definitely YES because:

Looking at the timeline SpaceX have:

- more and more launches in total

- more and more successful launches

- less failed launches

- more deliverd kilograms

- more unique customers



# Hopefully see you soon! #