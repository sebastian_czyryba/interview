from utils import utils
import pandas as pd
import unittest


class TestUtils(unittest.TestCase):

    def setUp(self):
        self.df = {"date_utc": ["2006-03-24T22:30:00.000Z", "2008-03-24T22:30:00.000Z", "2010-03-24T22:30:00.000Z"]}

    def test_get_min_date(self):
        dt = utils.get_first_start(pd.DataFrame(self.df))
        assert dt.year == 2006

    def test_get_max_date(self):
        dt = utils.get_last_start(pd.DataFrame(self.df))
        assert dt.year == 2010

