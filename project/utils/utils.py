import json
import pandas as pd
import os
from dateutil.parser import parse
from utils.logger import Logger

logger = Logger()


def get_data(data_name):
    try:
        with open(os.path.join(os.getcwd(), 'data', "{}.txt".format(data_name))) as json_file:
            data = json.load(json_file)
            df = pd.DataFrame(data)
            return df
    except IOError:
        logger.error("File {} doesn't exist".format(data_name))
        return exit(-1)


def get_first_start(df):
    return parse(min(df['date_utc']))


def get_last_start(df):
    return parse((max(df['date_utc'])))