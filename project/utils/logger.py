import os
from datetime import datetime


class Logger:

    def __init__(self):
        self.log_file = os.path.join(os.getcwd(), "log.txt")

    def _save(self, msg):
        try:
            print(msg)
            with open(self.log_file, 'a') as log_file:
                log_file.write(msg + '\n')
        except IOError as e:
            print('Cannot save log msg: {}'.format(e))

    def info(self, msg):
        log = "{} INFO: {}".format(datetime.now(), msg)
        self._save(log)

    def error(self, msg):
        log = "{} ERROR: {}".format(datetime.now(), msg)
        self._save(log)