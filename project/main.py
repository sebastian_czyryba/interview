from reports import ReportSpaceXProgress


if __name__ == "__main__":
    space_x_report = ReportSpaceXProgress()
    space_x_report.generate_report()