import pandas as pd
from utils import utils
import matplotlib.pyplot as plt
import numpy as np
import os
from utils.logger import Logger
from _collections_abc import ABCMeta, abstractmethod
import glob


class Report(metaclass=ABCMeta):

    def __init__(self):
        self.logger = Logger()

    @abstractmethod
    def generate_report(self):
        self.logger.info("Generating report - START")
        df_results = self._calculate_results()
        self._plot_results(df_results)
        self._create_report_file()
        self.logger.info("Generating report - DONE")

    @abstractmethod
    def _get_output_model(self):
        return True

    @abstractmethod
    def _calculate_results(self):
        return True

    @abstractmethod
    def _plot_results(self, data_frame):
        return True

    @abstractmethod
    def _create_report_file(self):
        return True


class ReportSpaceXProgress(Report):

    def __init__(self):
        super().__init__()
        self.metrics = ['TotalLaunches', 'SuccessfulLaunches',
                        'FailedLaunches', 'UniqueCustomers', 'PayloadsMassKgs']
        self.df_launches = utils.get_data('launches')
        self.df_payloads = utils.get_data('payloads')
        self._first_start = utils.get_first_start(self.df_launches)
        self._last_start = utils.get_last_start(self.df_launches)

    def generate_report(self):
        super().generate_report()

    def _get_output_model(self):
        results = {'Date': [], 'TotalLaunches': [], 'SuccessfulLaunches': [], 'FailedLaunches': [],
                   'UniqueCustomers': [], 'UpcomingLaunches': [], 'PayloadsMassKgs': []}
        return results

    def _calculate_results(self):
        try:
            self.logger.info("Calculating results")
            dfs_merged = pd.merge(self.df_launches, self.df_payloads, left_on='id', right_on='launch', how='left')
            df_filtered = dfs_merged[['success', 'date_utc', 'customers', 'upcoming', 'mass_kg']]
            df_filtered['date_utc'] = pd.to_datetime(df_filtered['date_utc'])

            results_model = self._get_output_model()

            for year in range(self._first_start.year, self._last_start.year, 1):
                df_per_year = df_filtered[df_filtered['date_utc'].dt.year == year]
                if len(df_per_year) != 0:
                    successful_launches_count = df_per_year[df_per_year['success']
                                                            & (df_per_year['upcoming'] == 0)].sum()['success']
                    unique_customers_count = len(
                        list(set(df_per_year[df_per_year['upcoming'] == 0]['customers'].sum())))
                    upcoming_launches_count = len(df_per_year[df_per_year['upcoming'] == 1])
                    failed_launches_count = len(df_per_year[df_per_year['success'] == False])
                    total_launches = len(df_per_year)
                    payloads_mass_kg_per_year = df_per_year[df_per_year['success']
                                                            & (df_per_year['upcoming'] == 0)]["mass_kg"].sum()

                    results_model['Date'].append(year)
                    results_model['TotalLaunches'].append(total_launches)
                    results_model['SuccessfulLaunches'].append(successful_launches_count)
                    results_model['FailedLaunches'].append(failed_launches_count)
                    results_model['UniqueCustomers'].append(unique_customers_count)
                    results_model['UpcomingLaunches'].append(upcoming_launches_count)
                    results_model['PayloadsMassKgs'].append(payloads_mass_kg_per_year)

            self.logger.info("Calculating results - DONE")
            return pd.DataFrame(results_model)
        except Exception as e:
            self.logger.error("Results calculation failed! ERROR: {}".format(e))
            exit(-1)

    def _plot_results(self, df):
        try:
            self.logger.info("Generating plots")
            for metric in self.metrics:
                self.logger.info("Generating plot - {}".format(metric))
                plt.style.use('ggplot')
                plt.hist(df['Date'], weights=df[metric],
                         bins=np.arange(self._first_start.year, self._last_start.year, 0.5), align='left')
                plt.title(metric)
                plt.xlabel('Date')
                output_path = os.path.join(os.getcwd(), 'report')
                if not os.path.isdir(output_path):
                    os.mkdir(output_path)
                plt.savefig(os.path.join(os.getcwd(), 'report', "{}.png".format(metric)), dpi=100)
                plt.close()
            self.logger.info("Generating plots - DONE")
        except Exception as e:
            self.logger.error("Plots generation failed! ERROR: {}".format(e))
            exit(-1)

    def _create_report_file(self):
        try:
            self.logger.info("Creating HTML file with report")
            plots = glob.glob(os.path.join(os.getcwd(), 'report', '*.png'))

            description = '''                    
                            The program presents space-x missions progress.<br>
                            Report describes in general way achievements and improvements between 2006-2020.<br><br>
        
                            Results based on open space-x API. I took into consideration two endpoints:<br>
                                - payloads<br>
                                - launches<br><br>
        
                            Question is general so I decided to choose 5 quite general metrics:<br>
                                - Successful launches in every year between 2006 and 2020<br>
                                - Failed launches in every year between 2006 and 2020<br>
                                - Total count launches in every year between 2006 and 2020<br>
                                - Unique customers in every year between 2006 and 2020<br>
                                - Payloads mass in kgs delivered in payloads in every year between 2006 and 2020<br><br>
        
                            All metrics are presented in histograms so in the first glace end user can see progress.<br>
                            Trend is visable in each histogram and data shows improvements in each chosen category.<br><br>
        
                                <b>"Is the space program doing okay?"<br><br>
        
                                I would answer: definitely YES because:<br><br>
        
                                Looking at the timeline, SpaceX have:<br>        
                                    - more and more launches in total<br>       
                                    - more and more successful launches<br>    
                                    - less failed launches<br>
                                    - more delivered kilograms<br>
                                    - more unique customers (increasing interest in services) <br></b>
                        '''
            body = ''
            for plot in plots:
                body += '''
                        <div style="float:left; margin:10px">
                          <img src="{}">
                        </div>
                        '''.format(os.path.basename(plot))

            template = '''<!DOCTYPE html>
                        <html>
                          <head>
                            <title>SpaceXReport</title>
                          </head>
                          <body>
                          <table align="center" border="0.5">
                          <th>
                            DESCRIPTION
                          <th>
                          <tr>
                            <td style="text-align:left">{}</td>
                          </tr>
                        </table>
                            {}
                          </body>
                        </html>'''.format(description, body)

            with open(os.path.join(os.getcwd(), 'report', "report.html"), 'w') as html_file:
                html_file.write(template)
            self.logger.info("Creating HTML file with report - Done")
        except Exception as e:
            self.logger.error("HTML file creation failed! ERROR: {}".format(e))
